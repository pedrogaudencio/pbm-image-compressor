#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define LINE_SIZE 71
int size_x, size_y;
FILE * uncompressed_file;
char * filename;
char * sufixo=".zipped";


/* recebe um byte e devolve apenas a parte correspondente à cor (1 bit) */
int descodifica_cor(unsigned char byte){
    return byte>>7;
}


/* recebe um byte e devolve apenas a parte correspondente à quantidade (7 bits) */
int descodifica_tamanho(unsigned char byte){
    byte<<=1;
    byte>>=1;
    return byte;
}


/* escreve para um novo ficheiro a imagem descomprimida */
void write(unsigned char * uncompressed, int c_pos)
{
    char * fname = strstr(filename,sufixo);
    strncpy(fname,"\0",2);
    printf("%s",fname);
    uncompressed_file = fopen(fname, "w+");
    if (!uncompressed_file)
    {
        printf("Erro ao criar o ficheiro!\n");
    }
    else
    {
        int i=0;
        while(i<c_pos){
            fprintf(uncompressed_file,"%c",uncompressed[i]);
            i++;
        }
    }
        printf("Ficheiro descomprimido: %s\n",fname);
        fclose(uncompressed_file);
}


/* converte um inteiro para a sua representação em binário (serve apenas para mostrar resultados no terminal, não influencia o funcionamento
do programa) retirado do Stackoverflow: http://stackoverflow.com/questions/111928/is-there-a-printf-converter-to-print-in-binary-format#answer-112956 */
const char * int_to_binary(int x)
{
    static char b[9];
    b[0] = '\0';

    int z;
    for(z=128;z>0;z>>=1)
        strcat(b, ((x & z) == z) ? "1" : "0");

    return b;
}


void organiza(char * buffer)
{

}


void read(char * name)
{
    FILE *f = fopen(name, "r");

    int buff_size;
    fseek(f, 0, SEEK_END);
    buff_size = ftell(f);
    rewind(f);

    int * buff=(int *)malloc(sizeof(int)*buff_size*sizeof(int));
    char * buff_converted=(char *)malloc(sizeof(char)*buff_size*sizeof(int)*8);

    //printf("buff size: %d\n",buff_size);

    rewind(f);
    int atual = ftell(f);

    int bit8, bit7, bit6, bit5, bit4, bit3, bit2, bit1;
    char * bits=(char *)malloc(sizeof(char));
    bool ativo_x=false, ativo_y=false;

    int go_buffer=0;
    while((int)atual != EOF){
        buff[go_buffer]=(unsigned) atual;

        if(go_buffer==0 && descodifica_cor(atual)==0){
            size_x+=descodifica_tamanho(atual);
            ativo_x=true;
        }
        else if((ativo_x==true) && descodifica_cor(atual)==0 && descodifica_tamanho(atual)!=0)
            size_x+=descodifica_tamanho(atual);
        else if((ativo_y==true) && descodifica_cor(atual)==0 && descodifica_tamanho(atual)!=0)
            size_y+=descodifica_tamanho(atual);
        else if((ativo_x==false) && (ativo_y==false))
            for(int i=0;i<=descodifica_tamanho(atual);i++){
                bit1 = (unsigned) descodifica_cor(atual) & 1;
                bit2 = ((unsigned) descodifica_cor(atual) >>1) & 1;
                bit3 = ((unsigned) descodifica_cor(atual) >>2) & 1;
                bit4 = ((unsigned) descodifica_cor(atual) >>3) & 1;
                bit5 = ((unsigned) descodifica_cor(atual) >>4) & 1;
                bit6 = ((unsigned) descodifica_cor(atual) >>5) & 1;
                bit7 = ((unsigned) descodifica_cor(atual) >>6) & 1;
                bit8 = ((unsigned) descodifica_cor(atual) >>7) & 1;

                //printf("%d%d%d%d%d%d%d%d", bit8,bit7,bit6,bit5,bit4,bit3,bit2,bit1);
                sprintf(bits, "%d%d%d%d%d%d%d%d", bit8, bit7, bit6, bit5, bit4, bit3, bit2, bit1);
                //printf("%s",bits);
                strcat(buff_converted,(char *)bits);
            }

        //printf("buffer[%d]: %d  \t\t ftell(f): %ld",go_buffer,(int)atual, ftell(f));

        //printf("[%d]:%d\n",descodifica_cor(atual),descodifica_tamanho(atual));

        go_buffer++;

        atual = fgetc(f);

        if((ativo_x==true) && descodifica_cor(atual)==0 && descodifica_tamanho(atual)==0){
            ativo_x=false;
            ativo_y=true;
        }
        else if ((ativo_y==true) && descodifica_cor(atual)==0 && descodifica_tamanho(atual)==0)
            ativo_y=false;

        //printf("\tnext: %d\n",(int)atual);
    }

    //printf("size x=%d\n",size_x);
    //printf("size y=%d\n",size_y);

    printf("%s\n",buff_converted);

    //fclose(f);

    //organiza(buff_converted);
}


int main(int argc, char *argv[])
{
    if(argc!=2)
        printf("utilização: %s <nome_do_ficheiro>.pbm.zipped\n", argv[0]);
    else{
        filename=argv[1];
        read(filename);
    }

    return 0;
}
