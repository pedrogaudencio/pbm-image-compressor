#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define LINE_SIZE 71
int size_x, size_y;
FILE * compressed_file;
char * filename;
char * sufixo=".zipped";


/* efetua o calculo da entropia e das respetivas probabilidades de 1 e 0 */
void calcula_entropia(int * contagem, int c_pos)
{
    double zeros = 0.0, uns = 0.0, total = 0.0, p1 = 0.0, p0 = 0.0, entropia;

    for(int i=2;i<c_pos;i+=2){
        if(contagem[i]==0)
            zeros+=contagem[i+1];
        else
            uns+=contagem[i+1];
    }

    total = zeros+uns;
    p1 = uns/total;
    p0 = zeros/total;

    entropia=-((p1*(log2(p1)))+(p0*(log2(p0))));
    printf("Entropia (valor calculado): %lf\nP(1)=%lf\nP(0)=%lf\n\n",entropia,p1,p0);

}


/* escreve para um novo ficheiro a imagem comprimida */
void write(unsigned char * compressed, int c_pos)
{
    compressed_file = fopen(strcat(filename,sufixo), "w+");
    if (!compressed_file)
    {
        printf("Erro ao criar o ficheiro!\n");
    }
    else
    {
        int i=0;
        while(i<c_pos){
            fprintf(compressed_file,"%c",compressed[i]);
            i++;
        }
    }
        printf("Ficheiro comprimido: %s\n",filename);
        fclose(compressed_file);
}


/* recebe a cor (1 ou 0) e coloca no bit à esquerda (mais significativo) e a sua quantidade (com um máximo de 7 bits - sequência de 127 simbolos) */
unsigned converte(int cor, int n)
{
    unsigned char byte = 0;
    //printf("cor: %d | n: %d\n",cor,n);
    byte=cor;
    //printf("byte=cor: %x\n",byte);
    byte<<=7;
    //printf("byte<<7: %x\n",byte);
    byte|=n;
    byte=byte|n;
    //printf("byte|=n: %x\n",byte);
    return byte;
}


/* converte um inteiro para binário (serve apenas para mostrar resultados no terminal)
retirado do Stackoverflow: http://stackoverflow.com/questions/111928/is-there-a-printf-converter-to-print-in-binary-format#answer-112956 */
const char * int_to_binary(int x)
{
    static char b[9];
    b[0] = '\0';

    int z;
    for(z=128;z>0;z>>=1)
        strcat(b, ((x & z) == z) ? "1" : "0");

    return b;
}


/* comprime para um array de ints, primeiro o tipo (0 ou 1) e depois a quantidade (counter), sempre sequencialmente até ao fim do buffer, alternando as posições
   nota: as primeiras posições estão reservadas para o tamanho da imagem (colunas x linhas) e o critério de separação são dois zeros: (0,0) */
void organiza(char * buffer, int buff_size)
{
    int counter = 1, c_pos = 2, counter_sacanas = 0, counter_x = 0, size_x_extra = 0, counter_y = 0, size_y_extra = 0, extra_xy = 0, all_together[size_x*size_y];
    all_together[0]=size_x;
    all_together[1]=size_y;
    char current = buffer[0];

    for(int i=0;i<(int)buff_size;i++){
        if(current==buffer[i+1]){
            //printf("atual (%c) | próximo (%c) - iguais\n", current, buffer[i+1]);
            counter++;
        }
        else{
            //printf("atual (%c) | próximo (%c) - diferentes\n", current, buffer[i+1]);
            all_together[c_pos]=current-'0';
            c_pos++;
            all_together[c_pos]=counter;
            c_pos++;
            current=buffer[i+1];
            if(counter>127){
                //printf("if counter>127 (%d)\n",counter);
                counter_sacanas+=counter/127+1;
                //printf("counter_sacanas: %d\n",counter_sacanas);
                //printf("counter: %d\n",counter);
                if(counter%127>0)
                    counter_sacanas++; //counter_sacanas+=counter;
            }
            counter=1;
        }
    }

    /*
    printf("c_pos=%d\n",c_pos);
    for(int i=0;i<c_pos;i++){
      printf("(%d,%d)\n", all_together[i], all_together[i+1]);
      i++;
    }*/

    //calcula_entropia(all_together,c_pos);

    if(size_x>127){
        //printf("if size_x>127 (%d)\n",size_x);
        counter_x+=size_x/127;
        //printf("counter_x: %d\n",counter_x);
        if((size_x-counter_x*127)>0){
            size_x_extra+=(size_x-counter_x*127);
            //printf("size_x_extra: %d\n",size_x_extra);
        }
        if(size_x_extra != 0)
            extra_xy++;
    }
    if(size_y>127){
        //printf("if size_y>127 (%d)\n",size_y);
        counter_y+=size_y/127;
        //printf("counter_y: %d\n",counter_y);
        if((size_y-counter_y*127)>0){
            size_y_extra+=(size_y-counter_y*127);
            //printf("size_y_extra: %d\n",size_y_extra);
        }
        if(size_y_extra != 0)
            extra_xy++;
    }

    unsigned char compressed[c_pos+counter_x+counter_y+extra_xy+2];

    int compress_index = 0;
    if(size_x>127){
        for(int i=counter_x;i>0;i--){
            compressed[compress_index]=converte(0,127);
            //printf("(0,%d)[%d] -> (%s) (size_x>127)\n",compressed[compress_index],compress_index,int_to_binary(compressed[compress_index]));
            compress_index++;
        }
        if(size_x_extra > 0){
            compressed[compress_index]=converte(0,size_x_extra);
            //printf("(0,%d)[%d] -> (%s) (size_x_extra)\n",compressed[compress_index],compress_index,int_to_binary(compressed[compress_index]));
            compress_index++;
        }
    }
    else{
        compressed[compress_index]=converte(0,size_x);
        //printf("(0,%d)[%d] -> (%s) (size_x)\n",compressed[compress_index],compress_index,int_to_binary(compressed[compress_index]));
        compress_index++;
    }
    compressed[compress_index]=converte(0,0);
    //printf("(0,%d)[%d] -> (%s) (size_x break)\n",compressed[compress_index],compress_index,int_to_binary(compressed[compress_index]));
    compress_index++;

    if(size_y>127){
        for(int i=counter_y;i>0;i--){
            compressed[compress_index]=converte(0,127);
            //printf("(0,%d)[%d] -> (%s) (size_y>127)\n",compressed[compress_index],compress_index,int_to_binary(compressed[compress_index]));
            compress_index++;
        }
        if(size_y_extra > 0){
            compressed[compress_index]=converte(0,size_y_extra);
            //printf("(0,%d)[%d] -> (%s) (size_y_extra)\n",compressed[compress_index],compress_index,int_to_binary(compressed[compress_index]));
            compress_index++;
        }
    }
    else{
        compressed[compress_index]=converte(0,size_y);
        //printf("(0,%d)[%d] -> (%s) (size_y)\n",compressed[compress_index],compress_index,int_to_binary(compressed[compress_index]));
        compress_index++;
    }
    compressed[compress_index]=converte(0,0);
    //printf("(0,%d)[%d] -> (%s) (size_y break)\n",compressed[compress_index],compress_index,int_to_binary(compressed[compress_index]));
    compress_index++;

    //printf("c_pos: %d\n",c_pos);
    c_pos+=counter_sacanas;
    //printf("c_pos+counter_sacanas: %d\n",c_pos);
    for(int i=compress_index;i<=c_pos;i++){
        if(all_together[i+1]>127){
            int c = 0, sacana = all_together[i+1];
            while(sacana>127){
                c=counter_sacanas/127;
                //printf("c: %d\n",c);
                for(int j=0;j<c;j++){
                    //printf("c: %d\ncounter: %d\n", c, counter);
                    compressed[i]=converte(all_together[i],127);
                    sacana-=127;
                    i++;
                }
                if((sacana%127)>0){
                    sacana-=c*127;
                    sacana-=sacana%127;
                    compressed[i]=converte(all_together[i],sacana);
                    i++;
                }
            }
        }
        compressed[i]=converte(all_together[i],all_together[i+1]);
        //printf("(%d,%d)[%d]->(%d)\n",all_together[i],all_together[i+1],i,compressed[i]);
        i++;
    }

    write(compressed,c_pos);

    /*
    for(int i=compress_index;i<c_pos;i++){
        printf("(%d,%d)[%d]->(%s)\n",all_together[i],all_together[i+1],i,int_to_binary(compressed[i]));
        //printf("%s",int_to_binary(compressed[i]));
        //printf("%c",compressed[i]);
        i++;
    }*/
}


/* recebe uma string e um caracter, sempre que encontra esse caracter na string remove-o
   por fim adiciona o caracter '\0' para anunciar o novo final da string */
void remove_char(char * string, char c)
{
   int i, j = 0;

   for(i=0;string[i]!='\0';i++){
      if(string[i]!=c){
         string[j++] = string[i];
      }
   }

   string[j]='\0';
}


/* recebe o ficheiro e carrega primeiro as dimensões separadamente, só depois carrega
   o resto do ficheiro para o buffer */
void read(char * name)
{
    FILE *f = fopen(name, "r");

    int buff_size;
    fseek(f, 0, SEEK_END);
    buff_size = ftell(f);
    rewind(f);

    char line[LINE_SIZE];

    // salta para a 2a linha
    for(int i = 0;i<2;i++){
        fgets(line, LINE_SIZE, f);
    }

    char size[LINE_SIZE];
    memcpy(size,line,sizeof(line));

    char * sz;
    sz=strtok(size," ");
    size_x = atoi(sz);
    sz=strtok(NULL," ");
    size_y = atoi(sz);

    //printf("size x=%d\n",size_x);
    //printf("size y=%d\n",size_y);

    int * buff=(int *)malloc(sizeof(int)*buff_size);
    char * buff_converted=(char *)malloc(sizeof(char)*buff_size);

    //printf("buff size: %d\n",buff_size);

    rewind(f);
    int atual = ftell(f);

    int bit8, bit7, bit6, bit5, bit4, bit3, bit2, bit1;
    char * bits=(char *)malloc(sizeof(char)*8);

    int go_buffer=0;
    while((int)atual != EOF){
        buff[go_buffer]=(unsigned) atual;
        go_buffer++;

        bit1 = (unsigned) atual & 1;
        bit2 = ((unsigned) atual >>1) & 1;
        bit3 = ((unsigned) atual >>2) & 1;
        bit4 = ((unsigned) atual >>3) & 1;
        bit5 = ((unsigned) atual >>4) & 1;
        bit6 = ((unsigned) atual >>5) & 1;
        bit7 = ((unsigned) atual >>6) & 1;
        bit8 = ((unsigned) atual >>7) & 1;

        //printf("buffer[%d]: %d \t ftell(f): %ld",go_buffer,(int)atual, ftell(f));
        //printf("%d%d%d%d%d%d%d%d", bit8,bit7,bit6,bit5,bit4,bit3,bit2,bit1);
        sprintf(bits, "%d%d%d%d%d%d%d%d", bit8, bit7, bit6, bit5, bit4, bit3, bit2, bit1);
        //printf("%s",bits);
        strcat(buff_converted,(char *)bits);

        atual = fgetc(f);
        //printf("\tnext: %d\n",(int)atual);
    }

    //printf("%s\n",buff_converted);

    //fclose(f);

    organiza(buff_converted, buff_size);

}


int main(int argc, char *argv[])
{
    if(argc!=2)
        printf("utilização: %s <nome_do_ficheiro>.pbm\n", argv[0]);
    else{
        filename=argv[1];
        read(filename);
    }

    return 0;
}
